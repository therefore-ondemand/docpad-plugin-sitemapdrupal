# History

## v2.4.0 2017 April 20
- Updated dependencies

## v2.3.0 2017 April 20
- Updated base files

## v2.2.0 2012 December 13
- Added the ability to customise which collection is used
	- Thanks to [brockfanning](https://github.com/brockfanning) for [pull request #4](https://github.com/docpad/docpad-plugin-sitemap/pull/4)

## v2.1.0 2012 December 13
- Will now error if `templateData.site.url` is not defined
- Added the ability to customise the sitemap location via the `filePath` plugin configuration option
	- Thanks to [Nathan Bowser](https://github.com/nathanbowser) for [pull request #6](https://github.com/docpad/docpad-plugin-sitemap/pull/6)
- Updated dependencies
- Repackaged

## v2.0.1 2012 October 2
- Updated dependencies

## v0.2.0 2012 December 4
- Begun fork of https://github.com/DjebbZ/docpad-plugin-sitemap
